import datetime
import json
import logging
import os
import sched
import time

import dotenv
import telegram
from jinja2 import Template
from telegram.ext import CommandHandler, Updater

# Filename for the users storage
user_json_filename = "users.json"

# Messages folder
messages_folder = "messages"

# Messages declaration
messages_json_filename = "messages.json"

messages = []
users = []

scheduler = sched.scheduler(time.time)


class User:
    """Class for storing information about a subscribed user
    """
    def __init__(self, name: str, chat_id: int, start_time: int):
        """Initialize User object

        Args:
            chat_id (int): User (chat) id
            start_time (int): Time when user subscribed
        """
        self.chat_id = chat_id
        self.start_time = start_time
        self.name = name

        self.params = {"name": name, "link": "https://static.osetr.su/test.html"}

    def to_dict(self):
        """Return object in a dictionary form

        Returns:
            dict: Dictionry of current user
        """
        return {
            "chat_id": self.chat_id,
            "start_time": self.start_time,
            "name": self.name,
        }

    @classmethod
    def from_dict(cls, data: dict):
        """Create user object from JSON dictionary

        Args:
            data (dict): Dictionary with user data

        Returns:
            User: new user
        """
        return cls(**data)

    def schedule_messages(self):
        """Schedules all messages
        """

        # Timestamp of the last message
        last_timestamp = self.start_time

        current_time = time.time()

        # Loop through every message
        for message in sorted(messages, key=lambda x: x.order):

            logging.debug("Checking message #%s", message.order)

            # Time when the message will be sent
            send_time = message.delay + last_timestamp
            last_timestamp = send_time

            # Message is to be delivered or should've been delivered up to 2 seconds ago
            # Second condition helps when send_time and current_time are close
            if send_time >= current_time or abs(send_time - current_time) <= 2:
                logging.debug(
                    "Scheduling message to run @ %s",
                    datetime.datetime.fromtimestamp(send_time),
                )

                # Use scheduler to schedule message sending
                scheduler.enterabs(
                    time=send_time,
                    action=message.create_send_handler(
                        self.chat_id, params=self.params
                    ),
                    priority=0,
                )


class MessagePart:
    """Superclass for storing a part of a message. Should not be created directly
    """
    def __init__(self, filename: str):
        """Initialize a new object

        Args:
            filename (str): Path to the related file
        """
        self.path = os.path.join(messages_folder, filename)

    def send(self, bot: telegram.Bot, chat_id: int, params: dict):
        """Send this message to specified client

        Args:
            bot (telegram.Bot): Bot to use when sending
            chat_id (int): Chat ID to send to
            params (dict): Templating parameters
        """
        pass


class TextMessagePart(MessagePart):
    """Message part which stores text
    """
    def __init__(self, filename: str):
        """Initialize a new object

        Args:
            filename (str): Path to the text file
        """
        super().__init__(filename)

        with open(self.path, "rt") as f:
            self.template = Template(f.read())

    def send(self, bot: telegram.Bot, chat_id: int, params: dict):
        bot.send_message(text=self.template.render(**params), chat_id=chat_id)


class ImageMessagePart(MessagePart):
    """Message part which stores image
    """
    def send(self, bot: telegram.Bot, chat_id: int, params: dict):
        bot.send_photo(chat_id=chat_id, photo=open(self.path, "rb"))


class PromoMessage:
    """Object for storing one message instance and working with it
    """

    # Conversion dictionary: units to seconds
    units_dict = {"second": 1, "minute": 60, "hour": 3600}

    message_part_types = {"text": TextMessagePart, "image": ImageMessagePart}

    def __init__(self, delay: int, message_parts: list, order: int):
        """Initialize a new Message object

        Args:
            delay (int): Delay from the previous message
            message_parts (list): Message parts to include
            order (int): Message order number. Currently not used: just in case.
        """
        self.delay = delay
        self.order = order
        self.message_parts = message_parts

        self.bot = None

    @classmethod
    def from_dict(cls, data: dict):
        """Converts a dict from JSON into PromoMessage

        Args:
            data (dict): Dictionary from JSON

        Raises:
            RuntimeError: Unknown message part type or delay unit

        Returns:
            PromoMessage: New instance of PromoMessage class
        """

        message_parts = []

        # Go through every message part
        for message_part in data["body"]:
            part_class = PromoMessage.message_part_types.get(message_part["type"])

            if part_class is None:
                raise RuntimeError(
                    "Unknown message part type: {}".format(message_part["type"])
                )

            message_parts.append(part_class(message_part["filename"]))

        # Calculate delay in seconds
        delay = data["delay"]

        multiplier = PromoMessage.units_dict.get(data["unit"])

        if multiplier is None:
            raise RuntimeError("Unknown delay unit: {}".format(data["unit"]))

        delay *= multiplier

        return cls(delay=delay, message_parts=message_parts, order=data["order"])

    def create_send_handler(self, chat_id: int, params: dict):
        """Create a new event handler for the scheduler. Returns a function.

        Args:
            chat_id (int): Chat ID to respond to
            params (dict): Parameters for the formatter
        """
        def inner_handler():
            
            # Sequentially send every message part
            for message in self.message_parts:
                message.send(self.bot, chat_id, params)

        return inner_handler

    def connect_bot(self, bot: telegram.Bot):
        """Add a Telegram bot to the message instance

        Args:
            bot (telegram.Bot): Bot instance
        """
        self.bot = bot


def start_handler(update: telegram.Update, context):
    """Simple handler for the start command
    """

    new_user = User(
        chat_id=update.effective_chat.id,
        start_time=time.time(),
        name=update.message.chat.first_name,
    )

    # Only react to new users
    if new_user.chat_id not in [u.chat_id for u in users]:
        logging.debug("New user with id #%s", new_user.chat_id)
        users.append(new_user)

        new_user.schedule_messages()

        # This exists to not rewrite existing JSON-file
        if os.path.exists(user_json_filename):
            mode = "r"
        else:
            mode = "w"

        with open(user_json_filename, mode + "t+") as f:
            json.dump([user.to_dict() for user in users], f, indent=4)
    else:
        logging.debug("Old user, ignoring")


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)

    logging.info("Loading message data from filesystem")

    messages_json_path = os.path.join(messages_folder, messages_json_filename)

    if not os.path.exists(messages_json_path):
        raise RuntimeError("Messages JSON file not found")

    with open(messages_json_path, "rt") as f:
        raw_msg_json = json.load(f)

    for n, msg_dict in enumerate(raw_msg_json):
        logging.debug("Loading message #%s", n + 1)
        messages.append(PromoMessage.from_dict(msg_dict))

    logging.debug("Messages loaded")

    # Load up users (if they exist)
    if os.path.exists(user_json_filename):
        logging.debug("Found users, loading")

        with open(user_json_filename, "rt") as f:
            raw_user_json = json.load(f)

        for user_dict in raw_user_json:
            users.append(User.from_dict(user_dict))

        logging.debug("Users loaded")

    else:
        logging.debug("Users not found")

    logging.info("Initializing Telegram bot")

    # Loading Telegram API key
    dotenv.load_dotenv("secrets.env")

    tg_token = os.getenv("TG_API_KEY")

    # Telegram magic
    updater = Updater(token=tg_token, use_context=True)

    dispatcher = updater.dispatcher

    start_handler = CommandHandler("start", start_handler)

    dispatcher.add_handler(start_handler)

    for message in messages:
        message.connect_bot(updater.bot)

    for user in users:
        user.schedule_messages()

    logging.info("Bot initialized, starting")

    updater.start_polling()

    # Infinite loop - main program cycle
    while True:
        scheduler.run()
        time.sleep(0.5)
