# Python Telegram Scheduler

Subscribe every user to a timed message list. 

## Installing

Program looks into `messages` folder to find `messages.json` file which describes all the messages that should be sent.
Refer to `messages_example.json` for an example.

Before the first launch you will need to create `messages` folder with a specified JSON-file and a `secrets.env` file in the root,
which holds the Telegram API token. The file should be in a classic ENV format:

```bash
TG_API_KEY=123:abcdef
```

All the required libraries are specified in the `requirements.txt` file.

## Usage

Just run it